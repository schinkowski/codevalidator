//
//  CodeValidator.h
//  CodeValidator
//

#import <Foundation/Foundation.h>

@interface CodeValidator : NSObject

/**
 *  Iterates through .m files in the directory and subdirectories and replaces any occurences
 *  of 'self' within blocks with 'weakSelf' and creates a 'weakSelf' variable outside the block.
 *
 *  @param classesDirectory		Directory to check
 */
- (void)runWithDirectory:(NSString*)classesDirectory;

@end
