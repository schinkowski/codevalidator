//
//  CodeValidator.m
//  CodeValidator
//

#import "CodeValidator.h"

@implementation CodeValidator

- (void)runWithDirectory:(NSString*)classesDirectory {
	NSDirectoryEnumerator *fileEnumerator = [[NSFileManager defaultManager] enumeratorAtPath:classesDirectory];
	NSString *pathName = nil;
	while (pathName = [fileEnumerator nextObject]) {
		if ([[pathName pathExtension] isEqualToString:@"m"]) {
			NSString *filename = [classesDirectory stringByAppendingPathComponent:pathName];
			// NSLog(@"Checking file: %@", filename);
			NSError *error;
			NSString *contents = [NSString stringWithContentsOfFile:filename encoding:NSMacOSRomanStringEncoding error:&error];
			if (error) {
				NSLog(@"Unable to open file: %@", filename);
				continue;
			}
			
			// Patterns that will be used with regex to find blocks.
			NSArray *patterns = @[@"\\^\\(.*\\).*",
								  @"\\^[ ]*\\{.*"];
			
			// Find blocks.
			BOOL contentsChanged = NO;
			for (NSString *pattern in patterns) {
				NSInteger rangeStart = 0;
				do {
					NSRange blockRange = [contents rangeOfString:pattern options:NSRegularExpressionSearch range:NSMakeRange(rangeStart, contents.length - rangeStart)];
					if (blockRange.location != NSNotFound) {
						NSString *blockStartString = [contents substringWithRange:NSMakeRange(blockRange.location, contents.length - blockRange.location)];
						NSString *blockString;
						NSInteger openingBraceCount = 0;
						NSInteger c;
						for (c = 0; c < blockStartString.length; c++) {
							unichar character = [blockStartString characterAtIndex:c];
							if (character == '{') {
								openingBraceCount++;
							}
							else if (character == '}') {
								openingBraceCount--;
								
								if (openingBraceCount == 0) {
									blockString = [blockStartString substringToIndex:c];
									break;
								}
							}
						}
						
						NSRange rangeOfSelfInBlock = [blockString rangeOfString:@"\\bself\\b" options:NSRegularExpressionSearch];
						
						if (rangeOfSelfInBlock.location != NSNotFound) {
							NSLog(@"Found self in a block in %@", filename);
							const NSInteger selfStart = blockRange.location + rangeOfSelfInBlock.location;
							NSString *weakSelf = @"weakSelf";
							const NSInteger selfLength = 4;
							NSString *changedContents = [contents stringByReplacingCharactersInRange:NSMakeRange(selfStart, selfLength) withString:weakSelf];
							
							if ([filename containsString:@"PPURacingEventViewController"]) {
								int x = 0;
								x++;
							}
							
							// Insert line to define weakSelf in the line before the block starts if there isn't one already.
							NSArray *newLinePatterns = @[@";", @"{", @"}"];
							NSInteger indexOf1stLineBeforeBlock = -1;
							for (NSString *newLinePattern in newLinePatterns) {
								NSInteger offset = [newLinePattern isEqualToString:@";"] ? 1 : 0;
								offset = 1;
								NSInteger index = [contents rangeOfString:newLinePattern options:NSBackwardsSearch range:NSMakeRange(0, blockRange.location)].location + offset;
								indexOf1stLineBeforeBlock = MAX(indexOf1stLineBeforeBlock, index);
							}
							NSInteger indexOf2ndLineBeforeBlock = [contents rangeOfString:@"\n" options:NSBackwardsSearch range:NSMakeRange(0, indexOf1stLineBeforeBlock)].location;
							
							NSRange weakSelfRange = [contents rangeOfString:weakSelf options:NSLiteralSearch range:NSMakeRange(indexOf2ndLineBeforeBlock, indexOf1stLineBeforeBlock - indexOf2ndLineBeforeBlock)];
							if (weakSelfRange.location == NSNotFound) {
								NSString *weakSelfDefinition = @"\n\t__weak typeof(self) weakSelf = self;";
								
								changedContents = [changedContents stringByReplacingCharactersInRange:NSMakeRange(indexOf1stLineBeforeBlock, 0) withString:weakSelfDefinition];
							}
							
							contents = changedContents;
							contentsChanged = YES;
						}
						else {
							blockRange.length = c;
							rangeStart = blockRange.location + blockRange.length;
						}
					}
					else {
						rangeStart++;
						rangeStart = [contents rangeOfString:@"\n" options:NSCaseInsensitiveSearch range:NSMakeRange(rangeStart, contents.length - rangeStart)].location;
					}
				}
				while (rangeStart != NSNotFound);
			}
			
			if (contentsChanged) {
				NSError *error;
				[contents writeToFile:filename atomically:YES encoding:NSMacOSRomanStringEncoding error:&error];
				if(error) {
					NSLog(@"Unable to write to file: %@. \nError:%@", filename, error.description);
				}
			}
		}
	}
}

@end
