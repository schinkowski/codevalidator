//
//  AppDelegate.m
//  CodeValidator
//

#import "AppDelegate.h"
#import "CodeValidator.h"

// Set this to the direcory you want to check:
NSString *const classesDirectory = @"SET_ME";

@interface AppDelegate ()

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
	CodeValidator* validator = [CodeValidator new];
	[validator runWithDirectory:classesDirectory];

	exit(0);
}

@end
