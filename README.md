Checks objective-C files for use of self in blocks and changes them to weakSelf to avoid retain cycles. 

To use:
 
* Open AppDelegate.m

* Change classesDirectory to the directory on your machine that you want to run the app on

* Run the app